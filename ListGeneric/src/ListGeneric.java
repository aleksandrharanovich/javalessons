import java.util.List;

public class ListGeneric <T>
{

    private Element HeadElement = null; // инициализация первого элемента null
    private int ContainerSize = 0;

    private void DecrementSize()
    {
        ContainerSize--;
    }

    private void IncrementSize()
    {
        ContainerSize++;
    }

    private void ResetSize()
    {
        ContainerSize = 0;
    }

    private class Element
    {
        T Data;
        Element Prev;
        Element Next;
        int Index = -1;
    }

    private Element GetLastElement()
    {
        Element Current = HeadElement;
        while (Current != null)
        {
            if(Current.Next == null)
            {
                break;
            }
            Current = Current.Next;
        }
        return Current;
    }

    /**
     * @return Вернуть индекс последнего элемента
     */
    public int GetLastElementIndex()
    {
        Element LastElement = GetLastElement();
        return LastElement != null ? LastElement.Index : -1;
    }

    private Element GetElementByData (T Data)
    {
        Element Current = HeadElement;
        while (Current != null)
        {
            if (Current.Data.equals(Data))
            {
                break;
            }
            Current = Current.Next;
        }
        return Current;
    }


    private Element GetElementByIndex (int Index)
    {
        Element Current = HeadElement;
        while (Current != null)
        {
            if (Current.Index == Index)
            {
                break;
            }
            Current = Current.Next;
        }
        return Current;
    }

    private boolean IsCorrectData (T Data)
    {
        return Data != null;
    } // проверка на валидность Data

    private void DecreaseIndex (Element Current)
    {
        while (Current != null)
        {
            Current.Index--;
            Current = Current.Next;
        }
    }

    private void IncrementIndex (Element Current)
    {
        while (Current != null)
        {
            //Current.Index = Current.Index+1;
            Current.Index++; // смещаем ндекс
            Current = Current.Next; //перезаписываем то что в CurrentNext в Curren // что бы изменить все индексы у каждого следующего элемента
        }
    }

    public int Add (T Data)
    {
        if (!this.IsCorrectData(Data))
        {
            return -1;
        }

        Element NewElement = new Element();
        NewElement.Data = Data;

        Element LastElement = GetLastElement();
        if(LastElement != null)
        {
            LastElement.Next = NewElement;
            NewElement.Prev = LastElement;
            NewElement.Index = LastElement.Index + 1;
        }
        else
        {
            HeadElement = NewElement;
            HeadElement.Index = 0;
        }

        IncrementSize();
        return NewElement.Index;
    }

    public int RemoveByData (T Data)
    {
        Element Current = GetElementByData(Data);
        return RemoveByElement(Current) ? Current.Index : -1;
    }

    public boolean RemoveByIndex (int Index)
    {
        Element Current = GetElementByIndex(Index);
        return RemoveByElement(Current);
    }

    private boolean RemoveByElement(Element Current)
    {
        if (Current == null)
        {
            return false;
        }

        Element CurrentPrev = Current.Prev;
        Element CurrentNext = Current.Next;

        if (CurrentPrev != null)
        {
            CurrentPrev.Next = CurrentNext;
        }
        if (CurrentNext != null)
        {
            CurrentNext.Prev = CurrentPrev;
        }

        // Удаление нулевого элементо из листа

        if (Current.Index == 0)
        {
            HeadElement = CurrentNext;
        }

        this.DecreaseIndex(CurrentNext);
        DecrementSize();
        return true;
    }

    public boolean Clear ()
    {
        if (HeadElement == null)
        {
            return false;
        }
        HeadElement = null;
        ResetSize();
        return true;
    }

    /**
     * Вставка данных по индексу. Увеличивает индекс последующим элементам.
     * @param Index по которому произойдет вставка
     * @param Data данные которые нужно вставить
     * @param ForceSet true - для принудительной вставки в случае если элемент по индексу не найден
     * @return
     */
    public boolean Set (int Index, T Data, boolean ForceSet)
    {
        Element Current = GetElementByIndex(Index);
        // если по переданному индексу найден элемен
        if (Current != null)
        {
            Element element = new Element();
            element.Index = Index;
            element.Data = Data;

            Element CurrentPrev = Current.Prev;
            element.Prev = CurrentPrev;
            element.Next = Current;

            if (CurrentPrev !=null)
            {
                CurrentPrev.Next = element;
            }
            Current.Prev = element;

            IncrementIndex(Current);

            if (Index == 0)
            {
                HeadElement = element;
            }
            IncrementSize();
            return true;
        }
        // если по переданному индексу не найден элемент и ForceSet = true
        // это значит, принудительно добавить элемент
        else if (ForceSet)
        {
            // добавляем в конец
            if (Index >= 0)
                return Add(Data) >= 0;
            // добавляем в начало
            else
                return Set(0,Data,true);
        }
        return false;
    }

    /**
     * Перезапись
     * @param Index перезаписываемого элемента
     * @param NewData новые данные
     * @return true если данные перезаписались
     */
    public boolean Replace (int Index, T NewData)
    {
        Element Current = GetElementByIndex(Index);

        if (Current == null)
        {
            return false;
        }
        Current.Data = NewData;
        return true;
    }

    /**
     * Взять и удлить
     * @return данные последнего удаленного элемента
     */
    public T Pop ()
    {
        Element Current = this.GetLastElement();
        if (Current == null) return null;

        final T ReturnData = Current.Data;
        RemoveByElement(Current);
        return ReturnData;
    }

    public T GetDataByIndex(int Index)
    {
        if (HeadElement == null)
        {
            return null;
        }
        Element Current = GetElementByIndex(Index);
        return Current != null ? Current.Data : null;
    }

    /**
     * @return количество элементов листа
     */
    public int GetLength ()
    {
        Element Current = HeadElement;
        int Counter = 0;
        while (Current != null)
        {
            Current = Current.Next;
            Counter++;
        }
        return Counter;
    }

    /**
     * Функция работает без перебора всех элементов
     * Использует хешированное значение размера
     * @return количество элементов листа
     */
    public int GetLengthQuick()
    {
        return ContainerSize;
    }

    /**
     * Меняет местами элементы по переданным индексам
     * @param A индекс
     * @param B индекс
     * @return возвращает true в случае успеха
     */
    public boolean Swap (int A, int B)
    {
        if (A==B || GetLength() <=1 || (GetDataByIndex(A) == null || GetDataByIndex(B) == null))
            return false;

        T AData = GetDataByIndex(A);
        Replace(A, GetDataByIndex(B));
        Replace(B, AData);
        return true;
    }
}