import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Проверка функционала ListGeneric
 */
public class JunitTest
{

    // Тестовые данные, что бы использовать в тестах
    final String TempData = "Test Data";

    @Test
    public void TestSwap() throws Exception
    {
        final int MaxCounter = 100;
        ListGeneric <Integer> List = new ListGeneric<>();
        for (int i=0; i<MaxCounter; i++)
            List.Add(i);
        int Length = List.GetLength();

        // # 1 - Функция отработала успешно с корректными данными
        assertEquals(true, List.Swap(1, 2));

        // # 2 - Колличество элементов не изменилось
        assertEquals(Length, List.GetLengthQuick());

        // # 3 - Функция отработала успешно с не корректными данными
        assertEquals(false, List.Swap(0, Length));

        // # 4 - Функция отработала успешно с отрицательными индексами
        assertEquals(false, List.Swap(-1,-2));

        // # 5 - Функция отработала успешно с одинакомыми индексами
        assertEquals(false, List.Swap(0,0));

        // # 6 - Функция отработала успешно с одинакомыми индексами
        assertEquals(false, List.Swap(10, 10));
    }

    /**
     * Проверка функции добавления элементов в ListGeneric
     * @throws Exception
     */
    @Test
    public void testAdd() throws Exception
    {
        ListGeneric<String> List = new ListGeneric<>();

        // # 1 - check added index element
        int result = List.Add(TempData);
        assertEquals(0, result);

        // # 2 - check data by added index
        String StringResult = List.GetDataByIndex(0);
        assertEquals(TempData, StringResult);

        // # 3 - check numbers of elements in list
        int resultNumber = List.GetLength();
        assertEquals(1, resultNumber);
    }

    /**
     * Проверка функции Replace
     * @throws Exception
     */
    @Test
    public void testReplace() throws Exception
    {
        ListGeneric<String> List = new ListGeneric<>();
        List.Add(TempData);

        // # 1 - replace SUCCESS
        String NewReplaceData = "qwerty";
        assertEquals(true, List.Replace(0,NewReplaceData));

        // # 2 - check valid Data
        assertEquals(NewReplaceData, List.GetDataByIndex(0));
    }

    /**
     * Проверка функции Pop
     * @throws Exception
     */
    @Test
    public void testPop() throws Exception
    {
        ListGeneric<String> List = new ListGeneric<>();
        List.Add(TempData);

        // # 1 - Check last indexes
        int LastIndex = List.GetLastElementIndex();
        List.Pop();
        assertEquals(1,  Math.abs(LastIndex - List.GetLastElementIndex()));
    }

    /**
     *Проверка функции Pop, стресс тест
     * @throws Exception
     */
    @Test
    public void StressTestPop() throws Exception
    {
        final int Max_Counter = 100;
        ListGeneric<String> List = new ListGeneric<>();
        for (int i=0; i<Max_Counter; i++)
        {
            List.Add(TempData);
        }

        for (int i=0; i<Max_Counter; i++)
        {
            List.Pop();
        }
        assertEquals(0, List.GetLength());
    }

    /**
     * Проверка функции Set
     * @throws Exception
     */
    @Test
    public void TestSet() throws Exception
    {
        ListGeneric<String> List = new ListGeneric<>();
        for (int i=0; i < 5; i++)
        {
            List.Add(TempData + i);
        }

        // # 1 - Answer from function true or false

        assertEquals(true, List.Set(1, "TestData", false));

        // # 2 - Check GetLenght
        int Lenght = List.GetLength();
        List.Set(1,"TestData2", false);
        assertEquals(1, List.GetLength() - Lenght);

        // # 3 Check Data

        String NewTestData = "qwerty1234";
        List.Set(1, NewTestData, false);
        assertEquals(NewTestData, List.GetDataByIndex(1));
    }

    /**
     * Проверка функции Set с параметром Force (true, false) Проверяем правельно ли отрабатывает принудительная вставка элементов
     * @throws Exception
     */
    @Test
    public void TestSetForce() throws Exception
    {
        ListGeneric<String> List = new ListGeneric<>();
        // # 1 - Принудительная вставка по нулевому индексу в пустой массив
        assertEquals(true, List.Set(0,TempData,true));


        List.Clear();
        // # 2 - Проверка что после выполнения функции Clear в List нет данных
        assertEquals(0,List.GetLength());

        // # 3 - В пустой массив по индексу 0 с параметром ForceSet = false, вставка не произойдет
        assertEquals(false, List.Set(0,TempData,false));

        // # 4 - Произойдет вставка в случает отрицательного индекса и ForceSet = true
        assertEquals(true, List.Set(-100,TempData,true));

        // # 5 - После принудительной вставки колличесво элементов изменилось на 1
        assertEquals(1,List.GetLength());

        // # 6 - Вставка не произойдет по несуществующему индексу, при Force - false.
        List.Clear();
        assertEquals(false, List.Set(1000,TempData,false));

        // # 6 - Вставка произойдет по несуществующему индексу, при Force - true.
        assertEquals(true, List.Set(1000,TempData,true));

        // # 7 - После принудительной вставки колличесво элементов изменилось на 1
        assertEquals(1,List.GetLength());
    }

    /**
     * Проверка функции Remove
     * @throws Exception
     */
    @Test
    public void TestRemove() throws Exception
    {
        final int MaxCounter = 1000;
        ListGeneric <Integer> List = new ListGeneric<>();

        // Создаем Лист состоящих из колличества = MaxCounter элементов
        for (int i=0; i<MaxCounter; i++)
            List.Add(i);

        // # 1 - Элемент заменен по переданной Data
        for (int i=MaxCounter-1; i>=0; i--)
        {
            assertEquals(true, List.RemoveByData(i)>=0);
        }

        for (int i=0; i<MaxCounter; i++)
            List.Add(i);

        // # 2 - Элемент заменен по переданному Index
        for (int i=MaxCounter-1; i>=0; i--)
        {
            assertEquals(true, List.RemoveByIndex(i));
        }
    }
    @Test
    public void TestDecreaseIndex() throws Exception
    {
        final int MaxCounter = 100;
        ListGeneric <Integer> List = new ListGeneric<>();

        // Создаем Лист состоящих из колличества = MaxCounter элементов
        for (int i=0; i<MaxCounter; i++)
            List.Add(i);

        // # 1 - Разница в колличестве элементов =1
        int Lenght = List.GetLength();
        List.RemoveByIndex(1);
        assertEquals(1,  Math.abs(List.GetLength()-Lenght));

        // # 2 -  Есть данные в последнем элементе после удаления
        assertEquals(true, List.GetDataByIndex(List.GetLength()-1) != null);

        // # 3 - Данных нет по последнему индексу до удаления
        assertEquals (null, List.GetDataByIndex(Lenght-1));
    }

    /**
     * Последний индекс листа верный
     * @throws Exception
     */
    @Test
    public void TestGetLastElementIndex() throws Exception
    {
        final int MaxCounter = 100;
        ListGeneric <Integer> List = new ListGeneric<>();

        // Создаем Лист состоящих из колличества = MaxCounter элементов
        for (int i=0; i<MaxCounter; i++)
            List.Add(i);

        // # 1 - сравнение длины листа-1 и индекса
        assertEquals(List.GetLength()-1, List.GetLastElementIndex());
    }

    @Test
    public void TestGetLengthQuick() throws Exception
    {
        final int MaxCounter = 100;
        ListGeneric <Integer> List = new ListGeneric<>();
        // # 1 - При создании Листа элементов 0
        assertEquals(0, List.GetLengthQuick());

        for (int i=0; i<MaxCounter; i++)
            List.Add(i);

        // # 2 - Два разных функционала проверки длины возвращают одни данные
        assertEquals(List.GetLength(), List.GetLengthQuick());

        // # 3 - Проверяем кол-во созданных элементов
        assertEquals(MaxCounter, List.GetLengthQuick());

        // # 4 - После удаления правельная длина
        List.RemoveByIndex(0);
        assertEquals(MaxCounter-1, List.GetLengthQuick());

        // # 5 - После удаления функции Pop правельная длина
        List.Pop();
        assertEquals(MaxCounter-2, List.GetLengthQuick());

        List.Clear();

        // # 6 - После очистки элементов нет
        assertEquals(0, List.GetLengthQuick());
    }

    @Test
    public void TestIncreaseIndex() throws Exception
    {
        final int MaxCounter = 100;
        ListGeneric <Integer> List = new ListGeneric<>();
        for (int i=0; i<MaxCounter; i++)
            List.Add(i);

        // # 1 - Сравнение длины до довавления элемента и после
        int Length = List.GetLength();
        List.Add(0);
        assertEquals(Length+1, List.GetLength());

        // # 2 -
        assertEquals(List.GetLastElementIndex(), Length);
    }

    @Test
    public void GlobaleTruesTest() throws Exception
    {
        final int MaxCounter = 100;
        ListGeneric <Integer> List = new ListGeneric<>();
        for (int i=0; i<MaxCounter; i++)
            assertEquals(List.Add(i), i);

        assertEquals(true, List.Swap(0,30));
        assertEquals(true, List.RemoveByIndex(1));
        assertEquals(true, List.Set(3,900,true));
        assertEquals(true, List.Pop() != null);
        assertEquals(true, List.Clear());

        assertEquals(0,List.GetLengthQuick());
        assertEquals(0, List.GetLength());
    }

    @Test
    public void GlobaleNegativeTest() throws Exception
    {
        final int MaxCounter = 100;
        ListGeneric <Integer> List = new ListGeneric<>();
        for (int i=0; i<MaxCounter; i++)
            assertEquals(List.Add(i), i);

        assertEquals(false, List.Swap(0,-3));
        assertEquals(false, List.RemoveByIndex(MaxCounter));
        assertEquals(false, List.Set(MaxCounter,900,false));
        assertEquals(true, List.Clear());
        assertEquals(false, List.Clear());
        assertEquals(true, List.Pop() == null);

        assertEquals(0,List.GetLengthQuick());
        assertEquals(0, List.GetLength());
    }
}