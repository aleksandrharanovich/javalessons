

public class StaticCat {

    public static class Cat {
        private String mName;
        private int mAge;
        static int sId = 0;
        static int sTestNumber = 8;

        public Cat(String name, int age) {
            mName = name;
            mAge = age;
            sId++;

            System.out.println(Cat.sId + "");
            System.out.println(Cat.sTestNumber + "");

            // Создаём первого кота
            Cat firstCat = new Cat("vasya", 2);
            System.out.println(Cat.sId + "");
            System.out.println(Cat.sTestNumber + "");

            Cat.sTestNumber = 15; // устанавливаем новое значение
            // Создаём второго кота
            Cat secondCat = new Cat("murzik", 12);
            System.out.println(Cat.sId + "");
            System.out.println( Cat.sTestNumber + "");
        }
        @Override
        public String toString() {
            return this.mName;
        }
    }
}
