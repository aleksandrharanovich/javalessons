/**
 * Singlton
 */
class Manager
{
    private static Manager manager = null; // создаем приватную статическую переменную manager типа Manager. Инициализируем ее


    public static Manager Get() // создаем статическую функцию
    {

        if (manager == null) // устовие, при котором создасться переменная manager
        {
            manager = new Manager();
        }

        return manager; // возвращаем переменную по вызову.
    }

    public void IncrementMyContaner()
    {
        //MyContainerCounter++;
        //MyContainerCounter = MyContainerCounter + 1;
        setMyContainerCounter(getMyContainerCounter()+1);
    }

    public void IncrementShoppingCounter()
    {
        //ShoppingCounter++;

        //ShoppingCounter = ShoppingCounter + 1;

        setShoppingCounter(getShoppingCounter() + 1);
    }

    public int getMyContainerCounter() {
        return MyContainerCounter;
    }

    public void setMyContainerCounter(int NewMyContainerCounter)
    {
        MyContainerCounter = NewMyContainerCounter;
    }

    private int MyContainerCounter = 0; // публичная переменная

    public int getShoppingCounter()
    {
        return ShoppingCounter;
    }

    public void setShoppingCounter (int NewShoppingCounter)
    {
        NewShoppingCounter = ShoppingCounter;
    }

    private int ShoppingCounter = 0;
}
