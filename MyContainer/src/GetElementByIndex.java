public class GetElementByIndex {


    public static String GetElement (int Index, MyContainer Container) // спрашиваем элемент по индексу
    {

        MyContainer.Element Current = Container.FirstElement;

        while (Current != null)
        {
            boolean Result = Current.Index == Index;
            if (Result == true)
            {
                break;
            }
            Current = Current.Next;
        }
        return Current != null ? Current.Data : "err";
    }


}
