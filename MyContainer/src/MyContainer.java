public class MyContainer
{
    public static int MyContainersCounter1 = 0;

    public MyContainer()
    {
        Manager.Get().IncrementMyContaner();
        this.MyContainersCounter1++;
        Manager.Get().IncrementShoppingCounter();
    }

    class Element
    {
        public int Index = -1; // инициализируем индекс ячейки для хранения // для нумирации ячеек хранения
        public Element Prev = null; // инициализируем ссылку на предшествующий элемент текущему // для создания связи между ячейками
        public Element Next = null; // инициализируем ссылку на следующий элемент за текущем // для создания связи между ячейками
        public String Data = ""; // инициализируем место для хранения информации // для хранения информации
    }

    Element FirstElement = null; // инициализируем первый элемент. Он класса Element - содержит все поля соответствующего класса

    /**
     * Добавляет String в контейнер
     * @param NewData для добавления нового элемента
     * @return -1 если не добавлено, >=0 в случе успешного добавления
     */
    public int Add (String NewData) // создаем функцию Add. он будет добавлять информацию в ячейки. Принимает в себя информацию NewData типа String
    {
        Element element = new Element(); // создаем новую ячейку класса Element и называем ее element
        element.Data = NewData; // записываем в ячейку информацию
        element.Index = 0; // Устанавливаем индекс ячейки с информацией // делаем 0 т.к еэто начало отсчета

        if(FirstElement == null) //проверка на наличие информации. // если информация в ячейке по индексу есть, нужно создавать новую ячейку
        {
            FirstElement = element; //если это первая запись, то в FirstElement записываем информацию
        }
        else // если это не первая запись, то создаем переменную и записываем в нее переданную информацию
        {
            Element LastElement = GetLastElement();
            //Создаем связи
            element.Prev = LastElement; // записываем ссылку на последний элемент
            LastElement.Next = element; // передаем то что хранится в текущем элементе в виде ссылки в Следующий элемент
            element.Index = LastElement.Index +1; // увеличиваем индекс для текущекго элемента.
        }

      return element.Index; // возвращаем индекс текущего элемента в Index как результат что елемент добавлен
    }

    /**
     * Удаляет элемент и листа по содержанию
     * @param RemoveData - принимает параметр/ключ для удаления
     * @return - 1 если не добавлено, >=0 в случе успешного добавления
     */
    public int Remove (String RemoveData) // Создаем функцию Remove. Будет удалять элементы по содержанию
    {
        Element Current = GetElementByData(RemoveData);

        // проверяем найденный элемент и меняем сязязи, таким образом удаляя элемент
        if (Current != null) // проверяем содержится что либо в Current, если да то:
        {
            Element CurrentPrev = Current.Prev; // в переменную CurrentPrev записываем то сто содержится в Prev
            Element CurrentNext = Current.Next; // в переменную CurrentNext записываем то сто содержится в Next

            if (CurrentPrev != null) // проверка
            {
                CurrentPrev.Next = CurrentNext; // перезаписываем содержимое CurrentPrev
            }
            if (CurrentNext != null)
            {
                CurrentNext.Prev = CurrentPrev; // перезаписываем содержимое CurrentNext
            }

            DecreaseIndex(CurrentNext);
        }

        return (Current != null) ? (Current.Index) : (-1);
    }

    /**
     * Удаляет элемент из листа по индексу
     * @param Index
     * @return 1 если не добавлено, >=0 в случе успешного добавления
     */
    public boolean RemoveByIndex(int Index)
    {
        Element Current = GetElement(Index);

        boolean Result = Current != null;

        if (Current != null) // проверяем содержится что либо в Current, если да то:
        {
            // меняем связи
            Element CurrentPrev = Current.Prev; // в переменную CurrentPrev записываем то сто содержится в Prev
            Element CurrentNext = Current.Next; // в переменную CurrentNext записываем то сто содержится в Next

            if (CurrentPrev != null) // проверка
            {
                CurrentPrev.Next = CurrentNext; // перезаписываем содержимое CurrentPrev
            }
            if (CurrentNext != null)
            {
                CurrentNext.Prev = CurrentPrev; // перезаписываем содержимое CurrentNext
            }

            DecreaseIndex(CurrentNext);
        }

        return Result;

        //return (Current != null) ? (Current.Index) : (-1); // переписываем индекс
    }

    /** Делает дикремен всем индекса после удаления элемента
     * @param Current
     */
    private void DecreaseIndex (Element Current)
    {
        while (Current != null) // в цикле
        {
            //Current.Index = Current.Index-1;
            Current.Index--; // смещаем индекс
            Current = Current.Next; //перезаписываем то что в CurrentNext в Curren // что бы изменить все индексы у каждого следующего элемента
        }
    }

    private static void IncrementIndex (Element Current)
    {
        while (Current != null) // в цикле
        {
            //Current.Index = Current.Index+1;
            Current.Index++; // смещаем ндекс
            Current = Current.Next; //перезаписываем то что в CurrentNext в Curren // что бы изменить все индексы у каждого следующего элемента
        }
    }

    /** Находим элемент по индексу и возвращаем содержимое
     * @param Index
     * @return - Data
     */
    public String Get (int Index) // спрашиваем элемент по индексу
    {
        Element Current = GetElement(Index);
        return Current != null ? Current.Data : "";
    }

    /**
     * @param Index
     * @return
     */
    private Element GetElement (int Index)
    {
        Element Current = FirstElement;
        while (Current != null && Current.Index != Index)
        {
            Current = Current.Next;
        }
        return Current;
    }

     /** Устанавливаем значение по индексу, "раздвигая элементы"
     * @param Index
     * @param Data
     * @return
     */
    public boolean Set (int Index, String Data) //записываем элемент по нужному индексу
    {
        Element Current = GetElement(Index); // записываем в Current FirstElement

        if (Current != null) // проверяем ячейку Current, если там что то есть то:
        {
            Element element = new Element(); // создаем новый элемент
            element.Data = Data; // сохраняем информацию
            element.Index = Index; // присваиваем индекс

            Element CurrentPrev = Current.Prev; //
            element.Prev = CurrentPrev; // записываем в предыдущий элемент
            element.Next = Current;
            if (CurrentPrev != null)
            {
                CurrentPrev.Next = element;
            }
            Current.Prev = element;

            IncrementIndex(Current); //

            if (Index == 0)
            {
                FirstElement = element;
            }

            return true;
        }

        if (Index == 0)
        {
            //return this.Add(Data) >= 0 ? true : false;
            return Add(Data) >= 0;
        }
        return false;
    }

    /**
     * @return
     */
    public boolean Clear ()
    {
        if (FirstElement == null)
        {
            return false;
        }
        FirstElement = null;
        return true;
    }

    /**
     * @param Index
     * @param NewData
     * @return
     */
    public boolean Replace (int Index, String NewData)
    {
        Element Current = GetElement(Index);

        if(Current == null)
        {
            return false;
        }
        Current.Data = NewData;
        return true;
    }

    public String Pop () // удаляет и возвращает последний элемент
    {
        if (FirstElement == null) // проверка на наличие ячеек
        {
            return "";
        }

        // находим последний элемент
        Element Current = GetLastElement();

        Element CurrentPrev = Current.Prev; //переходим в предыдущий элемен что бы оборвать связь.
        CurrentPrev.Next = null; // удаляем связь
        return Current.Data;
    }

    private Element GetLastElement()
    {
        Element Current = FirstElement;
        while (Current.Next != null)
        {
            Current = Current.Next;
        }
        return Current;
    }

    private Element GetElementByData (String Data)
    {
        Element Current = FirstElement; // передаем то что содержится в первом элементе в переменную Current
        while (Current != null) // если В Current пусто то:
        {
            //находим нужный элемент по переданному параметру
            boolean Result = Current.Data == Data; //сравниваем что содержится в RemoveData с тем что находится в текущей ячейке
            if (Result == true) // если true то выходим из цикла, элемент найден
                break;

            Current = Current.Next; // передаем то что содержится в текущей ячейке, в ячейку для следующего элемента

        }
        return Current;
    }
}