import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculateTest {
        @Test
        public void testCalA() throws Exception {
            Calculate calculate = new Calculate();
            int n = calculate.calA(2, 2);
            assertEquals(4, n);
        }

        @Test
        public void testCalA1() throws Exception {
            Calculate calculate = new Calculate();
            int n = calculate.calA(2, 2);
            assertEquals(5, n);
    }

}
