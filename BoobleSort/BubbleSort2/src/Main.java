import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        int[] a = {87,77,50,1,7,8,1,0,8};

        for (int i = a.length - 1; i >= 2; i--) {
            boolean sort = true;

            for (int j = 0; j < i; j++) {

                if (a[j] > a[j + 1]) {
                    int temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                    sort = false;
                }
            }
            if (sort) {
                break;
            }
        }
        System.out.println("массив : " + Arrays.toString(a));
    }
}
